var width = window.innerWidth
var height = window.innerHeight
var centerP = [width / 2, height / 2]

function windowResized() {
  background(0,0,0)
  resizeCanvas(windowWidth, windowHeight)
  width = windowWidth
  height = windowHeight
  centerP = [width / 2, height / 2]
}

function setup() {
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB, 255)
  frameRate(30)
}

numCircles = 3
var t = 0

function draw() {
  orbitRadius = Math.min(width, height) * 0.15
  blendMode(BLEND)
  background(34, 15, 250)

  blendMode(EXCLUSION)

  for (var n = 1; n <= numCircles; n+=1){
    radius = ((2*orbitRadius) / numCircles) * n
    fill((n / numCircles)*256, 210, 256);
    noStroke()
    circle(centerP[0]+cos(n*t)*orbitRadius, centerP[1]+sin(n*t)*orbitRadius, radius)
    circle(centerP[0]-cos(n*t)*orbitRadius, centerP[1]-sin(n*t)*orbitRadius, radius)
    noFill()
    circleWidth = radius / 2
    stroke((n / numCircles)*256, 230, 256)
    strokeWeight(circleWidth)
    circle(...centerP, 2 * orbitRadius + radius - circleWidth)
  }
  t += 0.007
}
